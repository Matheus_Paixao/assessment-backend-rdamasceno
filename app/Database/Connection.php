<?php
namespace App\Database;

class Connection {

	/**
	 * @var Connectors\IConnector
	 */
	private $connector;

	/**
	 * @var \PDO
	*/
	private $pdo;

	/**
	 * @var Connection[]
	*/
	private static $instances = [];

	/**
	 * @var array $configs
	*/
	private $configs = [
		'dev'  => [
			'username'  => 'root',
			'password'  => 'root',
			'host' 		=> 'localhost',
			'database'  => 'store_db',
		],
		'test' => [
			'username'  => 'root',
			'password'  => 'root',
			'host' 		=> 'localhost',
			'database'  => 'store_db_test',
		]
	];

	/**
	 * @var string
	 */
	private $queryBuilder;

	/**
	 * Connection constructor.
	 *
	 * @param $name
	 */
	private function __construct ($name) {
		$this->connector 	= new Connectors\MysqlConnector();
		$this->queryBuilder = Query\MysqlBuilder::class;
		$this->pdo = $this->connector->connect ($this->configs[$name]);
	}

	/**
	 * @param string $name
	 * @return static
	 */
	public static function getInstance ($name = 'dev') {

		if (!isset(static::$instances[$name])) {
			static::$instances[$name] = new static($name);
		}

		return static::$instances[$name];
	}

	/**
	 * @return mixed|\PDO
	 */
	function getPDO () {
		return $this->pdo;
	}

	/**
	 * @return Query\Builder
	 */
	function newQueryBuilder() {
		return new $this->queryBuilder($this);
	}

 	/**
	 * @param $query
	 * @param array $bindings
	 * @return \PDOStatement
	 */
	function makeStatement ($query, $bindings = []) {

		$stmt = $this->getPDO ()->prepare ($query);
		foreach ($bindings as $index =>  $binding) {
			$stmt->setAttribute ($index, $binding);
		}

		return $stmt;
	}

	/**
	 * @return mixed
	 */
	function getLastInsertId () {
		return $this->getPDO ()->lastInsertId ();
	}

	/**
	 * @param $query
	 * @param $bindings
	 * @return \PDOStatement
	 * @throws \Exception
	 */
	function execute ($query, $bindings = []) {
		$stmt = $this->makeStatement ($query, $bindings);
		if (!$stmt->execute ()) {
			throw new \Exception($stmt->errorInfo ());
		}
		return $stmt;
	}

	/**
	 * Executa a query e retorna um resultado.
	 *
	 * @param  string $query
	 * @param  array $bindings
	 * @return mixed
	 * @throws \Exception
	 */
	public function selectOne ($query, $bindings = []) {
		return $this->execute ($query, $bindings)->fetch (\PDO::FETCH_ASSOC);
	}

	/**
	 * Executa uma query e retorna os resultados encontrados.
	 *
	 * @param  string $query
	 * @param  array $bindings
	 * @return array
	 * @throws \Exception
	 */
	public function selectAll ($query, $bindings = []) {
		return $this->execute ($query, $bindings)->fetchAll (\PDO::FETCH_ASSOC);
	}
}