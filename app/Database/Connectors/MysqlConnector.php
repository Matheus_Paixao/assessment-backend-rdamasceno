<?php
namespace App\Database\Connectors;

class MysqlConnector implements IConnector {

	/**
	 * {@inheritdoc}
	 */
	function connect (array $config = []) {

		$username = $config['username'];
		$password = $config['password'];
		$host 	  = $config['host'];
		$db 	  = $config['database'];

		$pdo = new \PDO("mysql:dbname=$db;host=$host;port=3306", $username, $password);
		$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		return $pdo;
	}
}