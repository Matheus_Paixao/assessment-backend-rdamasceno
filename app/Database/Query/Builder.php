<?php
namespace App\Database\Query;

use App\Database\Connection;

abstract class Builder {

	/**
	 * @var Connection $connection
	*/
	protected $connection;

	protected $from    = '';

	protected $columns = [];

	protected $orders  = [];

	protected $wheres  = [];

	/**
	 * @var \Closure|null
	*/
	protected $transformer = null;

	/**
	 * Builder constructor.
	 *
	 * @param Connection $connection
	 */
	public function __construct (Connection $connection) {
		$this->connection = $connection;
	}

	/**
	 * @param \Closure|null $transformer
	 * @return Builder
	 */
	public function setTransformer (\Closure $transformer): Builder {
		$this->transformer = $transformer;

		return $this;
	}

	protected function clear () {
		$this->wheres  = [];
		$this->orders  = [];
		$this->columns = [];
	}

	/**
	 * @return array
	 */
	public function getWheres (): array {
		return $this->wheres;
	}

	/**
	 * @return array
	 */
	public function getOrders (): array {
		return $this->orders;
	}

	/**
	 * @return array
	 */
	public function getColumns (): array {
		return $this->columns;
	}

	/**
	 * @return string
	 */
	public function getFrom (): string {
		return $this->from;
	}

	/**
	 * @param string $from
	 * @return Builder
	 */
	public function setFrom (string $from): Builder {
		$this->from = $from;

		return $this;
	}

	/**
	 * @param $column
	 * @param null $operator
	 * @param null $value
	 * @param string $connector
	 * @return Builder
	 */
	abstract function where ($column, $operator = null, $value = null, $connector = 'and');

	/**
	 * @param $column
	 * @param null $operator
	 * @param null $value
	 * @return Builder
	 */
	function orWhere ($column, $operator = null, $value = null) {
		return $this->where ($column, $operator, $value, 'or');
	}

	/**
	 * @param string|array $column
	 * @param string $direction
	 * @return Builder
	 */
	abstract function orderBy ($column, $direction = 'ASC');

	/**
	 * @return array|mixed
	 */
	abstract function selectOne ();

	/**
	 * @return array
	 */
	abstract function selectAll ();

	/**
	 * @param array $attributes
	 * @return mixed
	 */
	abstract function create ($attributes = []);

	/**
	 * @param array $attributes
	 * @return bool
	 */
	abstract function update ($attributes = []);

	/**
	 * @return bool
	 */
	abstract function delete ();
}