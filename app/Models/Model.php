<?php
namespace App\Models;

use App\Database\Connection;
use App\Services\Utils;

abstract class Model {

	protected $primaryKey = 'id';

	private static $testing = false;

	/**
	 * Model constructor.
	 *
	 * @param array $attributes
	 */
	public function __construct (array $attributes = []) {
		$this->fill ($attributes);
	}

	/**
	 * @return string
	 */
	abstract function getTable ();

	/**
	 * @return array
	 */
	abstract function getFillable ();

	/**
	 * @param array $attributes
	 * @return Model
	 */
	function fill (array $attributes) {
		$fillable = array_merge ($this->getFillable (), [$this->getKeyName ()]);

		foreach ($attributes as $key => $value) {
			if (in_array ($key, $fillable)) {
				$this->setAttribute ($key, $value);
			}
		}

		return $this;
	}

	/**
	 * @param $column
	 * @return static
	 */
	function setAttribute ($column, $value) {
		$method = Utils::camelize("set-$column");
		if (method_exists ($this, $method)) {
			$this->{$column}($value);
		} else {
			$this->$column = $value;
		}
		return $this;
	}

	/**
	 * @param $column
	 * @return mixed
	 */
	function getAttribute ($column) {
		$method = Utils::camelize("get-$column");
		if (method_exists ($this, $method)) {
			return $this->{$method}();
		}
		return $this->$column;
	}

	/**
	 * @return array
	 */
	function toArray () {

		$attributes = [];
		if ($this->exists ()) {
			$attributes[$this->getKeyName ()] = $this->getKey ();
		}

		foreach ($this->getFillable () as $fillable) {
			$attributes[$fillable] = $this->getAttribute($fillable);
		}

		return $attributes;
	}

	/**
	 * @return string
	 */
	function getKeyName () {
		return $this->primaryKey;
	}

	/**
	 * @return mixed
	 */
	function getKey () {
		return $this->getAttribute ($this->getKeyName ());
	}

	/**
	 * @param $value
	 * @return void
	 */
	function setKey ($value) {
		$this->{$this->getKeyName ()} = $value;
	}

	/**
	 * @return bool
	 */
	function exists () {
		return !!$this->getKey ();
	}

	/**
	 * @return \App\Database\Query\Builder
	 */
	function newQueryBuilder () {

		if (self::$testing) {
			$conn = Connection::getInstance ('test');
		} else {
			$conn = Connection::getInstance ();
		}

		return $conn->newQueryBuilder ()
			->setTransformer (function($attributes){
				return new static($attributes);
			})
			->setFrom ($this->getTable ());
	}

	/**
	 * @param array $attributes
	 * @return static
	 */
	function create ($attributes = []) {

		$id = $this
			->fill ($attributes)
			->newQueryBuilder ()
			->create ($this->toArray ());

		$this->setKey ($id);
		return $this;
	}

	/**
	 * @param array $attributes
	 * @return bool
	 */
	function update ($attributes = []) {
		if ($this->exists()) {
			return $this
				->fill ($attributes)
				->newQueryBuilder ()
				->where ($this->getKeyName (), '=', $this->getKey())
				->update ($this->toArray ());
		}
	}

	/**
	 * @return bool
	 */
	function delete () {
		if ($this->exists()) {
			return $this->newQueryBuilder ()
				->where ($this->getKeyName (), '=', $this->getKey())
				->delete ();
		}
	}

	/**
	 * @return \App\Database\Query\Builder
	 */
	static function query () {
		return (new static)->newQueryBuilder ();
	}

	/**
	 * @param $id
	 * @return static|null
	 */
	function find ($id) {
		return static::query ()
			->where ($this->getKeyName (), '=', $id)
			->selectOne ();
	}

	/**
	 * @return static[]
	 */
	static function all() {
		return static::query ()->selectAll ();
	}

	static function testing (\Closure $fn) {
		try {
			self::$testing = true;

			$fn();

		} finally {
			self::$testing = false;
		}
	}
}