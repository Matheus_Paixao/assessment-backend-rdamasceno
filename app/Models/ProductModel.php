<?php
namespace App\Models;

class ProductModel extends Model {

	protected $id;

	protected $sku;

	protected $name;

	protected $description;

	protected $price;

	protected $quantity;

	protected $image;

	/**
	 * @return string
	 */
	function getTable () {
		return 'products';
	}

	/**
	 * @return array
	 */
	function getFillable () {
		return ['sku', 'description', 'price', 'quantity', 'image', 'name'];
	}

	/**
	 * @return mixed
	 */
	public function getId () {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId ($id) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getSku () {
		return $this->sku;
	}

	/**
	 * @param mixed $sku
	 */
	public function setSku ($sku) {
		$this->sku = $sku;
	}

	/**
	 * @return mixed
	 */
	public function getDescription () {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription ($description) {
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getPrice () {
		return +$this->price;
	}

	/**
	 * @return string
	 */
	public function getFormattedPrice (){
		return 'R$ '. number_format($this->getPrice (), 2, ',', ' ');
	}

	/**
	 * @param mixed $price
	 */
	public function setPrice ($price) {
		$this->price = $price;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity () {
		return $this->quantity;
	}

	/**
	 * @param mixed $quantity
	 */
	public function setQuantity ($quantity) {
		$this->quantity = $quantity;
	}

	/**
	 * @return mixed
	 */
	public function getImage () {
		return $this->image;
	}

	/**
	 * @param mixed $image
	 */
	public function setImage ($image) {
		$this->image = $image;
	}

	/**
	 * @return mixed
	 */
	public function getName () {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName ($name) {
		$this->name = $name;
	}
}