<?php
namespace App\Repositories;

class CategoryRepository extends Repository {

	protected $model = \App\Models\CategoryModel::class;

	/**
	 * @param $name
	 * @return \App\Models\CategoryModel|null
	 */
	function findByName ($name) {
		$model = null;
		if ($name) {
			$model = $this->newModel ()
				->newQueryBuilder ()
				->where ('name', '=', $name)
				->selectOne ();
		}

		return $model;
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	protected function validateAttributes (array $attributes, string $method) {
		$this->validate ([
			'code' => 'required',
			'name' => 'required'
		], $attributes);
	}

	/**
	 * @param \App\Database\Query\Builder $query
	 * @param $queryOptions
	 * @return \App\Database\Query\Builder
	 */
	protected function withSelectAllQuery ($query, $queryOptions) {

		if (isset($queryOptions['name'])) {
			$query->where('name', 'LIKE', "'%{$queryOptions['name']}%'");
		}

		if (isset($queryOptions['description'])) {
			$query->where('description', 'LIKE', "'%{$queryOptions['description']}%'");
		}

		return $query;
	}
}