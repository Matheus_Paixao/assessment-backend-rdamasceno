<?php
namespace App\Repositories\Concerns;

trait HasValidations {

	protected $errors = [];

	/**
	 * @param $rules
	 * @param $attributes
	 * @throws \Exception
	 */
	function validate ($rules, $attributes) {

		foreach ($rules as $key => $rule) {
			$validators = explode ('|', $rule);

			foreach ($validators as $validator) {
				$validator = trim ($validator);
				if (!method_exists ($this, $validator)) {
					throw new \Exception("Unknown validator $validator");
				}

				$this->$validator($key, $attributes[$key]??null);
			}
		}

		if (!empty($this->errors)){
			$errors = $this->errors;
			$this->errors = [];
			throw new ValidationException("Validation error", 400, $errors);
		}
	}

	function addError ($attr, $error){
		if (!isset($this->errors[$attr])) {
			$this->errors[$attr] = [];
		}
		$this->errors[$attr][] = $error;
	}

	function required ($key, $value) {
		if (empty($value) && !is_numeric ($value)) {
			$this->addError($key, 'This field is required');
		}
	}

	function numeric ($key, $value) {
		if (!is_numeric($value)) {
			$this->addError($key, 'This field must be numeric');
		}
	}
}