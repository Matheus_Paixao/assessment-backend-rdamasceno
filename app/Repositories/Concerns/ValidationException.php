<?php
namespace App\Repositories\Concerns;

use Throwable;

class ValidationException extends \Exception {

	protected $errors = [];

	public function __construct (string $message = "Validation error", int $code = 400, array $errors, Throwable $previous = null) {
		$this->errors = $errors;
		parent::__construct ($message, $code, $previous);
	}

	/**
	 * @return array
	 */
	public function getErrors (): array {
		return $this->errors;
	}
}