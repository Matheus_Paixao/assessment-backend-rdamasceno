<?php
namespace App\Repositories;

use App\Database\Connection;
use App\Models\Model;
use App\Services\File\Uploader;

class ProductRepository extends Repository {

	protected $model = \App\Models\ProductModel::class;

	/**
	 * @param $sku
	 * @return \App\Models\ProductModel|null
	 */
	function findBySku($sku) {

		$model = null;
		if ($sku) {
			$model = $this->newModel ()
				->newQueryBuilder ()
				->where ('sku', '=', $sku)
				->selectOne ();
		}

		return $model;

	}
	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	protected function validateAttributes (array $attributes, string $method) {
		$this->validate ([
			'sku'		  => 'required',
			'name'		  => 'required',
			'description' => 'required',
			'quantity'	  => 'required|numeric',
			'price'	  	  => 'required|numeric'
		], $attributes);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function withSelectAllQuery ($query, $queryOptions) {

		if (isset($queryOptions['sku'])) {
			$query->where('sku', '=', "'{$queryOptions['sku']}'");
		}

		if (isset($queryOptions['description'])) {
			$query->where('description', 'LIKE', "'%{$queryOptions['sku']}%'");
		}

		return $query;
	}

	/**
	 * Sincroniza as categorias do produto.
	 *
	 * @param \App\Models\ProductModel $product
	 * @param $categories
	 */
	private function syncCategories (\App\Models\ProductModel $product, $categories) {

		$pdo = Connection::getInstance ()->getPDO ();

		$stmt = $pdo->prepare ("DELETE FROM `products_categories` WHERE product_id = {$product->getKey ()}");
		$stmt->execute ();

		if (!empty($categories)) {
			$values = array_map (function($category) use($product){
				return "({$product->getKey()}, $category)";
			}, $categories);

			$values = implode (', ', $values);

			$stmt = $pdo->prepare ("INSERT INTO `products_categories` (product_id, category_id) VALUES $values");
			$stmt->execute ();
		}
	}

	/**
	 * @param $file
	 * @return null|string
	 * @throws \App\Services\File\Validators\ValidationException
	 * @throws \Exception
	 */
	function upload ($file) {

		if (!empty($file)) {
			$uploader = new \App\Services\File\Uploader();
			$uploader->validate ($file, 'image|maxSize:1000000');
			return $uploader->upload ($file, '/products');
		}

		return null;
	}
	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	function store (array $attributes): Model {

		$pdo = Connection::getInstance ()->getPDO ();

		try {
			$pdo->beginTransaction ();

			if (isset($attributes['image']) && empty($attributes['image']['tmp_name'])) {
				unset($attributes['image']);
			}

			if($image = $this->upload($attributes['image']??null)) {
				$attributes['image'] = $image;
			}

			/** @var \App\Models\ProductModel $model */
			$model = parent::store ($attributes);
			$this->syncCategories ($model, $attributes['categories']??[]);

			$pdo->commit ();

			return $model;

		} catch (\Exception $exception) {
			if ($pdo) {
				$pdo->rollBack ();
			}

			throw $exception;
		}
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	function update ($id, array $attributes = []): Model {

		$pdo = Connection::getInstance ()->getPDO ();

		try {
			$pdo->beginTransaction ();

			if (isset($attributes['image']) && empty($attributes['image']['tmp_name'])) {
				unset($attributes['image']);
			}

			if($image = $this->upload($attributes['image']??null)) {
				$attributes['image'] = $image;
			}

			/** @var \App\Models\ProductModel $model */
			$model = parent::update ($id, $attributes);
			$this->syncCategories ($model, $attributes['categories']??[]);

			$pdo->commit ();

			return $model;

		} catch (\Exception $exception) {
			if ($pdo) {
				$pdo->rollBack ();
			}

			throw $exception;
		}
	}

	/**
	 * Retorna as categorias do produto.
	 *
	 * @param \App\Models\ProductModel $productModel
	 * @return \App\Models\CategoryModel[]
	 * @throws \Exception
	 */
	function getCategories (\App\Models\ProductModel $productModel) {
		if (!$productModel->exists ()) {
			return [];
		}

		$stmt = Connection::getInstance ()
			->execute ("
				SELECT c.* FROM `categories` as c INNER JOIN products_categories pc ON pc.category_id = c.id
				WHERE pc.product_id = {$productModel->getKey ()}
			");

		$results = $stmt->fetchAll (\PDO::FETCH_ASSOC);

		return array_map (function ($result){
			return new \App\Models\CategoryModel($result);
		}, $results);
	}

	/**
	 * @param $id
	 * @return bool
	 * @throws \Exception
	 */
	function delete ($id): bool {
		/** @var \App\Models\ProductModel $model */
		$model = $this->findOrFail ($id);
		$model->delete ();

		if ($image = $model->getImage()) {
			$uploader = new Uploader();
			$uploader->delete ($image);
		}

		return true;
	}
}