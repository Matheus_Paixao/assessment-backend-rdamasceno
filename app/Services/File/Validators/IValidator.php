<?php
/**
 * Created by PhpStorm.
 * User: rodrigonevesdamasceno
 * Date: 14/02/2019
 * Time: 20:12
 */
namespace App\Services\File\Validators;

interface IValidator {

	/**
	 * @param array $file
	 * @param array ...$args
	 * @return string|null
	 */
	function validate ($file, ...$args);
}