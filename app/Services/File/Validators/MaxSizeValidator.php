<?php
namespace App\Services\File\Validators;

class MaxSizeValidator implements IValidator {

	function formatBytes($bytes, $precision = 2) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		// Uncomment one of the following alternatives
		// $bytes /= pow(1024, $pow);
		// $bytes /= (1 << (10 * $pow));

		return round($bytes, $precision) . ' ' . $units[$pow];
	}

	/**
	 * {@inheritdoc}
	 */
	function validate ($file, ...$args) {
		$maxSize  = $args[0]??null;
		$fileSize = $file['size']??null;

		if ($maxSize && $fileSize && $fileSize > $maxSize) {
			return "File size cannot be greater than {$this->formatBytes ($maxSize)}.";
		}

		return null;
	}
}