<script type="text/javascript">
    var elements = document.querySelectorAll('.input-text');
	elements.forEach(function (el) {
		el.addEventListener('focus', function (ev) {
			var info = ev.target.parentNode.querySelector('.input-info-danger');
			if (info) {
				ev.target.parentNode.removeChild(info);
			}
		});
    });
</script>
<footer>
	<div class="footer-image">
		<img src="/assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
		<span>go@jumpers.com.br</span>
	</div>
</footer>
</body>
</html>
