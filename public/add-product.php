<?php
require '../app/Views/Header.php';

$category_repo 	= new \App\Repositories\CategoryRepository();
$product_repo 	= new \App\Repositories\ProductRepository();

/**
 * @var \App\Models\CategoryModel[] $categories
*/
$categories = $category_repo->find ();
$product    = new \App\Models\ProductModel();

$id    	    = $_GET['id']??null;
$message	= [];
$validation_errors = [];

if (!empty($id)) {
	try {
		$product = $product_repo->findOrFail ($id);
	} catch (\Exception $exception) {
		echo '<script type="text/javascript">
				alert("'.$exception->getMessage ().'"); 
				window.location="/products.php";
			</script>';
	}
}

$product_categories = array_map (function($cat){
    return $cat->getKey();
}, $product_repo->getCategories ($product));

if (!empty($_POST)) {

	$attributes = [
		'sku' 		    => $_POST['sku']??null,
		'name' 		    => $_POST['name']??null,
		'price' 	    => $_POST['price']??null,
		'quantity' 	    => $_POST['quantity']??null,
		'description' 	=> $_POST['description']??null,
		'categories'    => $_POST['categories']??null
	];

	try {
		$product->fill ($attributes);
		$product_categories = $attributes['categories'];

		if ($product->exists ()) {
			$product = $product_repo->update ($product->getKey (), array_merge ($attributes, ['image'=> $_FILES['image']??null]));

		} else {
			$product_repo->store (array_merge ($attributes, ['image'=> $_FILES['image']??null]));
			$product = new \App\Models\ProductModel();
		}

		$message = [
			'type' 	  => 'success',
			'content' => 'Product successfully saved!'
		];

	} catch (\App\Repositories\Concerns\ValidationException $exception) {
		$validation_errors = $exception->getErrors ();
		$message = [
			'type' 	  => 'danger',
			'content' => $exception->getMessage (),
		];

	} catch (Exception $exception) {
		$message = [
			'type' 	  => 'danger',
			'content' => $exception->getMessage (),
		];
	}
}
?>
	<main class="content">
		<h1 class="title new-item"><?php echo $product->exists () ? 'Edit Product' : 'New Product';?></h1>

		<?php if (!empty($message)): ?>
            <div class="alert alert-<?php echo $message['type'];?>">
                <small><?php echo $message['content']; ?></small>
            </div>
		<?php endif; ?>

		<form method="post" action="" enctype="multipart/form-data">
			<div class="input-field">
				<label for="sku" class="label">Product SKU</label>
				<input type="text" id="sku" name="sku" class="input-text" value="<?php echo $product->getSku ();?>" /><br/>
				<?php if(isset($validation_errors['sku'])): ?>
					<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['sku']); ?>
					</span>
				<?php endif; ?>
			</div>

			<div class="input-field">
				<label for="name" class="label">Product Name</label>
				<input type="text" id="name" name="name" class="input-text" value="<?php echo $product->getName ();?>" />
				<?php if(isset($validation_errors['name'])): ?>
					<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['name']); ?>
					</span>
				<?php endif; ?>
			</div>

			<div class="input-field">
				<label for="price" class="label">Price</label>
				<input type="text" id="price" name="price" class="input-text" value="<?php echo $product->getPrice ();?>" />
				<?php if(isset($validation_errors['price'])): ?>
					<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['price']); ?>
					</span>
				<?php endif; ?>
			</div>

			<div class="input-field">
				<label for="quantity" class="label">Quantity</label>
				<input type="text" id="quantity" name="quantity" class="input-text" value="<?php echo $product->getQuantity ();?>" />
				<?php if(isset($validation_errors['quantity'])): ?>
					<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['quantity']); ?>
					</span>
				<?php endif; ?>
			</div>

			<div class="input-field">
				<label for="category" class="label">Categories</label>

				<select multiple id="category" name="categories[]" class="input-text">
					<?php
						foreach ($categories as $category):
							$selected = in_array ($category->getKey (), $product_categories);
					?>
                        <option value="<?php echo $category->getKey ()?>" <?php echo $selected ? 'selected' : ''; ?>>
                            <?php echo $category->getName ()?>
                        </option>

					<?php endforeach; ?>
				</select>
			</div>

			<div class="input-field">
				<label for="description" class="label">Description</label>
				<textarea id="description" name="description" class="input-text"><?php echo $product->getDescription ();?></textarea>
				<?php if(isset($validation_errors['description'])): ?>
					<span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['description']); ?>
					</span>
				<?php endif; ?>
			</div>

			<div class="input-field">


                <label for="image" class="label">Image</label>

                <div style="display:inline-block; max-width: 100px; vertical-align: middle">
                    <img src="<?php echo $product->getImage ()??'/assets/images/no-image.png';?>" title="Product Image" style="max-width: 100%; height: auto;" />
                </div>

                <div style="display:inline-block; vertical-align: middle">
                    <input type="file" name="image" id="image" accept="image/png, image/jpeg, image/gif, image/jpg" />
					<?php if(isset($validation_errors['image'])): ?>
                        <span class="input-info input-info-danger">
						<?php echo implode ('; ', $validation_errors['image']); ?>
					</span>
					<?php endif; ?>
                </div>
			</div>

			<div class="actions-form">
				<a href="/products.php" class="action back">Back</a>
				<input class="btn-submit btn-action" type="submit" value="Save Product" />
			</div>

		</form>
	</main>

<?php require '../app/Views/Footer.php'; ?>