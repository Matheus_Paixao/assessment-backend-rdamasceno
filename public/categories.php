<?php require '../app/Views/Header.php'; ?>
<main class="content">

	<div class="header-list-page">
		<h1 class="title">Categories</h1>
		<a href="add-category.php" class="btn-action">Add new Category</a>
	</div>

	<table class="data-grid">
		<tr class="data-row">
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Name</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Code</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Actions</span>
			</th>
		</tr>

		<?php
			$category_repo 	= new \App\Repositories\CategoryRepository();

			/**
			 * @var \App\Models\CategoryModel[] $categories
			*/
			$categories = $category_repo->find ();

			foreach ($categories as $category):
		?>

		<tr class="data-row">
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?php echo $category->getName ();?></span>
			</td>

			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?php echo $category->getCode ();?></span>
			</td>

			<td class="data-grid-td">
				<div class="actions">
					<div class="action edit"><a href="add-category.php?id=<?php echo $category->getKey ();?>">Edit</a></div>
                    <div class="action delete">
                        <a style="cursor:pointer;" class="btn-delete" data-id="<?php echo $category->getKey ();?>">Deletar</a>
                    </div>
				</div>
			</td>
		</tr>

		<?php endforeach;?>
	</table>
</main>

<script type="text/javascript">
    $('.btn-delete').on('click', function(evt){
        var target = $(evt.target)
        var id     = target.attr('data-id');

        if (confirm("Are you sure you want to delete this item?")) {
            $.ajax({
                method: "POST",
                url: "/delete-category.php",
                data: { id: id },
                dataType: 'json',
                success: (function( msg ) {
                    target.closest('.data-row').remove();
                    alert( msg.message );
                }),
                error: (function(jqXHR, textStatus, errorThrown) {
                    alert( 'Ops! An error occurred: ' + jqXHR.responseJSON.message );
                })
            });
        }
    });
</script>

<?php require '../app/Views/Footer.php'; ?>