<?php
require '../app/Views/Header.php';

$product_repo = new \App\Repositories\ProductRepository();

/**
 * @var \App\Models\ProductModel[] $products
*/
$products = $product_repo->find ();
?>

<main class="content">
	<div class="header-list-page">
		<h1 class="title">Dashboard</h1>
	</div>
	<div class="infor">
		You have <?php echo count($products)?> products added on this store: <a href="add-product.php" class="btn-action">Add new Product</a>
	</div>
	<ul class="product-list">

		<?php foreach($products as $product): ?>
            <li>
                <div class="product-image">
                    <img src="<?php echo $product->getImage()??'/assets/images/no-image.png'?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
                </div>
                <div class="product-info">
                    <div class="product-name"><span><?php echo $product->getName ();?></span></div>
                    <div class="product-price"><span class="special-price"><?php echo $product->getQuantity ();?> available</span> <span><?php echo $product->getFormattedPrice ()?></span></div>
                </div>
            </li>

        <?php endforeach; ?>
	</ul>
</main>

<?php require '../app/Views/Footer.php'; ?>
