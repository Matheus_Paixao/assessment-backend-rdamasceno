<?php require '../app/Views/Header.php'; ?>

<main class="content">

	<div class="header-list-page">
		<h1 class="title">Products</h1>
		<a href="/add-product.php" class="btn-action">Add new Product</a>
	</div>

	<table class="data-grid">

		<tr class="data-row">
			<th class="data-grid-th">
						<span class="data-grid-cell-content">Name</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">SKU</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Price</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Quantity</span>
			</th>
			<th class="data-grid-th">
				<span class="data-grid-cell-content">Categories</span>
			</th>

			<th class="data-grid-th">
				<span class="data-grid-cell-content">Actions</span>
			</th>
		</tr>

		<?php
            $repo = new \App\Repositories\ProductRepository;

            /**
             * @var \App\Models\ProductModel[] $products
             */
			$products = $repo->find ();

			foreach ($products as $product):
		?>

			<tr class="data-row">
				<td class="data-grid-td">
					<span class="data-grid-cell-content"><?php echo $product->getDescription ();?></span>
				</td>

				<td class="data-grid-td">
					<span class="data-grid-cell-content"><?php echo $product->getSku ();?></span>
				</td>

				<td class="data-grid-td">
					<span class="data-grid-cell-content"><?php echo $product->getFormattedPrice ();?></span>
				</td>

				<td class="data-grid-td">
					<span class="data-grid-cell-content"><?php echo $product->getQuantity (); ?></span>
				</td>

				<td class="data-grid-td">
					<span class="data-grid-cell-content">
                        <?php
                            $categories = $repo->getCategories ($product);
                            foreach ($categories as $category) {
                                echo $category->getName ()."<br />";
                            }
                        ?>
                    </span>
				</td>

				<td class="data-grid-td">
					<div class="actions">
						<div class="action edit">
                            <a href="add-product.php?id=<?php echo $product->getKey ();?>">Editar</a>
                        </div>
						<div class="action delete">
                            <a style="cursor:pointer;" class="btn-delete" data-id="<?php echo $product->getKey ();?>">Deletar</a>
                        </div>
					</div>
				</td>
			</tr>
		<?php
			endforeach;
		?>

	</table>
</main>
<script type="text/javascript">
    $('.btn-delete').on('click', function(evt){
    	var target = $(evt.target)
		var id     = target.attr('data-id');

		if (confirm("Are you sure you want to delete this item?")) {
			$.ajax({
				method: "POST",
				url: "/delete-product.php",
				data: { id: id },
				dataType: 'json',
				success: (function( msg ) {
					target.closest('.data-row').remove();
					alert( msg.message );
				}),
				error: (function(jqXHR, textStatus, errorThrown) {
					alert( 'Ops! An error occurred: ' + jqXHR.responseJSON.message );
				})
			});
        }
    });
</script>
<?php require '../app/Views/Footer.php'; ?>