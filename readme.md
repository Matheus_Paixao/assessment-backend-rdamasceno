# Test Backend - Web Jump
Projeto de teste de desenvolvimento backend para a Web Jump.

## Pré-requisitos
Antes de iniciar, certifique-se de que seu servidor possuí: 
- PHP 7+
- MYSQL 5+

## Passo 1: Instalando o projeto
Clone o projeto do bitbucket para sua máquina.

`<base_dir>$ git clone https://bitbucket.org/rdamasceno/assessment-backend.git`

Agora precisamos baixar as dependências do projeto e para isto vamos usar o composer. 
Caso ainda não possua o composer em sua máquina, instale-o seguindo as [instruções](https://getcomposer.org/doc/00-intro.md). 

No diretório raiz do projeto, execute o comando:

`<base_dir>/assessment-backend$ composer install`

## Passo 2: Criando o banco de dados

Crie dois banco de dados para seu projeto, um para o desenvolvimento e outro para os testes.
As configurações padrões estão definidas com os bancos *store_db* e *store_db_test*.

Importe em cada um dos bancos a sql localizada no diretório *<base_dir>/storage/database.sql*.

Via terminal: 

```
mysql -u root -p store_db < <base_dir>/storage/database.sql

mysql -u root -p store_db_test < <base_dir>/storage/database.sql
``` 

Acesse o arquivo *app/Database/Connection.php* para configurar os dados de acesso de sua base de dados.

```
	/**
	 * @var array $configs
	*/
	private $configs = [
		'dev'  => [
			'username'  => 'root',
			'password'  => 'root',
			'host' 	    => 'localhost',
			'database'  => 'store_db',
		],
		'test' => [
			'username'  => 'root',
			'password'  => 'root',
			'host' 	    => 'localhost',
			'database'  => 'store_db_test',
		]
	];
```

## Passo 3: Importando produtos e categorias
Utilizei o *symfony/console* para criar o comando que importa os produtos e categorias. Para executá-lo, acesse via terminal o diretório app/Commands e rode o comando:

`
./webjump import [--register_errors REGISTER_ERRORS] [--stop_on_fail [STOP_ON_FAIL]] <file>
`;

Os parâmetros *register_errors* e *stop_on_fail* são opcionais. Já o *file* é obrigatório, ele indica o caminho para o arquivo contendo os produtos.
Por padrão o arquivo de importação está localizado no diretório public/assets, então para executá-lo basta rodar comando:

`
<base_dir>/assessment-backend/app/Commands$ ./webjump import ../../public/assets/import.csv
`

## Passo 4: Testes
Utilizei o *phpunit* para os testes unitários. Eles estão testando apenas o repositório de produtos e seus principais métodos.
Para executá-lo, rode o comando: 

`
<base_dir>/assessment-backend$ vendor/bin/phpunit 
`